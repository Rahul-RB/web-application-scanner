from urllib2 import HTTPError

class WascanUnboundLocalError(UnboundLocalError):
	pass

class WascanDataException(Exception):
	pass

class WascanNoneException(Exception):
	pass

class WascanInputException(Exception):
	pass

class WascanGenericException(Exception):
	pass

class WascanConnectionException(HTTPError):
	pass

class WascanKeyboardInterrupt(KeyboardInterrupt):
	pass