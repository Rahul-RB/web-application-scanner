import sys
from ownChecks.checkSSL import checkSSL
from ownChecks.serverInfo import getServerInfo
# from ownChecks.serverInfo import validateServerSoftware
import subprocess
from lib.utils.printer import *
from lib.request.request import *
from plugins.fingerprint.cms.drupal import *
from plugins.fingerprint.cms.joomla import *
from plugins.fingerprint.cms.magento import *
from plugins.fingerprint.cms.plone import *
from plugins.fingerprint.cms.silverstripe import *
from plugins.fingerprint.cms.wordpress import *
from plugins.fingerprint.framework.asp_mvc import *
from plugins.fingerprint.framework.cakephp import *
from plugins.fingerprint.framework.codeigniter import *
from plugins.fingerprint.framework.cherrypy import *
from plugins.fingerprint.framework.dancer import *
from plugins.fingerprint.framework.django import *
from plugins.fingerprint.framework.flask import *
from plugins.fingerprint.framework.fuelphp import *
from plugins.fingerprint.framework.grails import *
from plugins.fingerprint.framework.horde import *
from plugins.fingerprint.framework.karrigell import *
from plugins.fingerprint.framework.larvel import *
from plugins.fingerprint.framework.nette import *
from plugins.fingerprint.framework.phalcon import *
from plugins.fingerprint.framework.play import *
from plugins.fingerprint.framework.rails import *
from plugins.fingerprint.framework.seagull import *
from plugins.fingerprint.framework.spring import *
from plugins.fingerprint.framework.symfony import *
from plugins.fingerprint.framework.web2py import *
from plugins.fingerprint.framework.yii import *
from plugins.fingerprint.framework.zend import *
from plugins.fingerprint.header.cookies import *
from plugins.fingerprint.header.header import *
from plugins.fingerprint.language.asp import *
from plugins.fingerprint.language.aspnet import *
from plugins.fingerprint.language.coldfusion import *
from plugins.fingerprint.language.flash import *
from plugins.fingerprint.language.java import *
from plugins.fingerprint.language.perl import *
from plugins.fingerprint.language.php import *
from plugins.fingerprint.language.python import *
from plugins.fingerprint.language.ruby import *
from plugins.fingerprint.os.bsd import *
from plugins.fingerprint.os.ibm import *
from plugins.fingerprint.os.linux import *
from plugins.fingerprint.os.mac import *
from plugins.fingerprint.os.solaris import *
from plugins.fingerprint.os.unix import *
from plugins.fingerprint.os.windows import *
from plugins.fingerprint.server.server import *
from plugins.fingerprint.waf.airlock import *
from plugins.fingerprint.waf.anquanbao import *
from plugins.fingerprint.waf.armor import *
from plugins.fingerprint.waf.aws import *
from plugins.fingerprint.waf.asm import *
from plugins.fingerprint.waf.baidu import *
from plugins.fingerprint.waf.barracuda import *
from plugins.fingerprint.waf.betterwpsecurity import *
from plugins.fingerprint.waf.bigip import *
from plugins.fingerprint.waf.binarysec import *
from plugins.fingerprint.waf.blockdos import *
from plugins.fingerprint.waf.ciscoacexml import *
from plugins.fingerprint.waf.cloudflare import *
from plugins.fingerprint.waf.cloudfront import *
from plugins.fingerprint.waf.comodo import *
from plugins.fingerprint.waf.datapower import *
from plugins.fingerprint.waf.denyall import *
from plugins.fingerprint.waf.dotdefender import *
from plugins.fingerprint.waf.edgecast import *
from plugins.fingerprint.waf.expressionengine import *
from plugins.fingerprint.waf.fortiweb import *
from plugins.fingerprint.waf.hyperguard import *
from plugins.fingerprint.waf.incapsula import *
from plugins.fingerprint.waf.isaserver import *
from plugins.fingerprint.waf.jiasule import *
from plugins.fingerprint.waf.knownsec import *
from plugins.fingerprint.waf.kona import *
from plugins.fingerprint.waf.modsecurity import *
from plugins.fingerprint.waf.netcontinuum import *
from plugins.fingerprint.waf.netscaler import *
from plugins.fingerprint.waf.newdefend import *
from plugins.fingerprint.waf.nsfocus import *
from plugins.fingerprint.waf.paloalto import *
from plugins.fingerprint.waf.profense import *
from plugins.fingerprint.waf.radware import *
from plugins.fingerprint.waf.requestvalidationmode import *
from plugins.fingerprint.waf.safe3 import *
from plugins.fingerprint.waf.safedog import *
from plugins.fingerprint.waf.secureiis import *
from plugins.fingerprint.waf.senginx import *
from plugins.fingerprint.waf.sitelock import *
from plugins.fingerprint.waf.sonicwall import *
from plugins.fingerprint.waf.sophos import *
from plugins.fingerprint.waf.stingray import *
from plugins.fingerprint.waf.sucuri import *
from plugins.fingerprint.waf.teros import *
from plugins.fingerprint.waf.trafficshield import *
from plugins.fingerprint.waf.urlscan import *
from plugins.fingerprint.waf.uspses import *
from plugins.fingerprint.waf.varnish import *
from plugins.fingerprint.waf.wallarm import *
from plugins.fingerprint.waf.webknight import *
from plugins.fingerprint.waf.yundun import *
from plugins.fingerprint.waf.yunsuo import *

class Fingerprint(Request):
	"""Fingerprint"""
	def __init__(self,kwargs,url):
		Request.__init__(self,kwargs)
		self.kwarg = kwargs
		self.url = url

	def run(self):
		info('Starting fingerprint target...')
		try:
			req = self.Send(url=self.url,method="GET")
			# 1.1. Detect Server
			s = server(self.kwarg,self.url).run()
			if s:
				plus('Server: %s'%(s))
			
			# 1.2. Detect Content Management Systems
			cms = detectCms(req.headers,req.content)
			for c in cms:
				if c != None and c != "":
					plus('CMS: %s'%(c))

			# 1.3. Detect Web Frameworks
			framework = detectFramework(req.headers,req.content)
			for f in framework:
				if f != None and f != "":
					plus('Framework: %s'%(f))
			
			# 1.4. Detect Language			
			lang = detectLanguage(req.content,self.url)
			for l in lang:
				if l != None and l != "":
					plus('Language: %s'%(l))
			
			# 1.5. Detect OS			
			os = detectOs(req.headers)
			for o in os:
				if o != None and o != "":
					plus('Operating System: %s'%o)
			
			# 1.6. Detect Web Appln Firewall			
			waf = detectWaf(req.headers,req.content)
			for a in waf:
				if a != None and a != "":
					plus('Web Application Firewall (WAF): %s'%a)

			# 1.7. Check SSL Certificate validity
			sslRes = checkSSL.checkSSL(url=self.url)
			if sslRes[0]==False:
				plus("SSL Certificate check result: %s"%"Invalid Certificate")
			else:
				plus("SSL Certificate check result: %s"%sslRes[1])

			# 1.8. Check Header Security			
			checkHeaders(req.headers,req.content)
			
			# 1.9. Validate Server software
			out,err = callValidateServerSoftware(self.url)
			# print out
			# print len(out)
			# print err
			i,j=0,0
			try:
				while i<3:
					if(out[i]!=False):
						if(out[i][0]>5.5):
							less("CVSS score of "+str(out[i][0])+" ,meaning it's a vulnerable software as: %s"%out[i][1])
							j+=1
					i+=1
			except:
				pass
			if(j==0):
				plus("Web software are all up-to-date and safe.")
			null()
		except Exception as e:
			print("Exception: {}".format(e))

def detectCms(headers,content):
	return (drupal(headers,content),
			joomla(headers,content),
			magento(headers,content),
			plone(headers,content),
			silverstripe(headers,content),
			wordpress(headers,content),
			)

def detectFramework(headers,content):
	return (
		cakephp(headers,content),
		cherrypy(headers,content),
		codeigniter(headers,content),
		dancer(headers,content),
		django(headers,content),
		flask(headers,content),
		fuelphp(headers,content),
		grails(headers,content),
		horde(headers,content),
		karrigell(headers,content),
		larvel(headers,content),
		mvc(headers,content),
		nette(headers,content),
		phalcon(headers,content),
		play(headers,content),
		rails(headers,content),
		seagull(headers,content),
		spring(headers,content),
		symfony(headers,content),
		web2py(headers,content),
		yii(headers,content),
		zend(headers,content)
		)

def checkHeaders(headers,content):
	if 'cookie' in headers.keys():
		if headers['cookie']:
			cookies().__run__(headers['cookie'])
	elif 'set-cookie' in headers.keys():
		if headers['set-cookie']:
			cookies().__run__(headers['set-cookie'])
	header()._run_(headers)

def callValidateServerSoftware(url):
	# path = "~/Desktop/PESU/PESU-6THSEM/CNS/project/working/WAScan/ownChecks/serverInfo/"
	# print "INSIDE THE FUNCTION"
	fullPath = "/home/rahul/Desktop/PESU/PESU-6THSEM/CNS/project/working/WAScan/ownChecks/serverInfo/validateServerSoftware.py"
	# cmd = "python3 "+path+"validateServerSoftware.py "+url
	cmd = "python3 "+fullPath+" "+url
	args = cmd.split()
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	finalOut,finalErr = None,None
	try:
		finalOut = eval(stdout)
		finalErr = eval(stderr)
	except:
		pass
	# print finalOut,finalErr
	return(finalOut,finalErr)
# def a(url):
# 	path = "~/Desktop/PESU/PESU-6THSEM/CNS/project/working/WAScan/ownChecks/serverInfo/"
# 	fullPath = "/home/rahul/Desktop/PESU/PESU-6THSEM/CNS/project/working/WAScan/ownChecks/serverInfo/validateServerSoftware.py"
# 	# cmd = "python3 "+path+"validateServerSoftware.py "+url
# 	cmd = "python3 "+fullPath+" "+url
# 	args = cmd.split()
# 	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
# 	stdout, stderr = process.communicate()
# 	return stdout,stderr
def detectLanguage(content,url):
	return (
		asp(content),
		aspnet(content),
		coldfusion(content),
		flash(content),
		java(content),
		perl(content),
		php(content),
		getServerInfo.printServerInfo(url),
		# validateServerSoftware.validateServer(url),
		python(content),
		ruby(content)
		)

def detectOs(headers):
	return (
		bsd(headers),
		ibm(headers),
		linux(headers),
		mac(headers),
		solaris(headers),
		unix(headers),
		windows(headers)
		)

def detectWaf(headers,content):
	return (
		airlock(headers,content),
		anquanboa(headers,content),
		armor(headers,content),
		asm(headers,content),
		aws(headers,content),
		baidu(headers,content),
		barracuda(headers,content),
		betterwpsecurity(headers,content),
		bigip(headers,content),
		binarysec(headers,content),
		blockdos(headers,content),
		ciscoacexml(headers,content),
		cloudflare(headers,content),
		cloudfront(headers,content),
		comodo(headers,content),
		datapower(headers,content),
		denyall(headers,content),
		dotdefender(headers,content),
		edgecast(headers,content),
		expressionengine(headers,content),
		fortiweb(headers,content),
		hyperguard(headers,content),
		incapsula(headers,content),
		isaserver(headers,content),
		jiasule(headers,content),
		knownsec(headers,content),
		kona(headers,content),
		modsecurity(headers,content),
		netcontinuum(headers,content),
		netscaler(headers,content),
		newdefend(headers,content),
		nsfocus(headers,content),
		paloalto(headers,content),
		profense(headers,content),
		radware(headers,content),
		requestvalidationmode(headers,content),
		safe3(headers,content),
		safedog(headers,content),
		secureiis(headers,content),
		senginx(headers,content),
		sitelock(headers,content),
		sonicwall(headers,content),
		sophos(headers,content),
		stingray(headers,content),
		sucuri(headers,content),
		teros(headers,content),
		trafficshield(headers,content),
		urlscan(headers,content),
		uspses(headers,content),
		varnish(headers,content),
		wallarm(headers,content),
		webknight(headers,content),
		yundun(headers,content),
		yunsuo(headers,content)
		)