import requests as rq

def getServerInfo(url):
	res = None
	try:
		res = rq.get(url)
		return res.headers
	except:
		# print("Invalid URL")
		pass

def printServerInfo(url):
	res = getServerInfo(url)
	try:
		return res['X-Powered-By']
	except:
		pass