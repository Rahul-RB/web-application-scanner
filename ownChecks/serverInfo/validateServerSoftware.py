from getServerInfo import getServerInfo
from pycvesearch import CVESearch
import sys

def validateServer(url):
	res = getServerInfo(url)
	# res = getServerInfo("https://pes.edu")
	phpRes = checkPHPInfo(res)
	apacheRes = checkApacheInfo(res)
	nginxRes = checkNginxInfo(res)
	finalRes = (phpRes,apacheRes,nginxRes)
	print(finalRes)
	return finalRes

def checkPHPInfo(res):
	numbers = ['0','1','2','3','4','5','6','7','8','9']
	cve =CVESearch()
	try:
		if "PHP" in res['X-Powered-By']:
			version = res['X-Powered-By'][4:]
			
			if(len(version)==0):
				version = "5.6.30"

			finalVer = ""
			for character in version:
				if((character in numbers) or (character=='.')):
					finalVer = finalVer+character
				else:
					break
			# print(finalVer)
			res = cve.cvefor('cpe:/a:php:php:'+finalVer)
			if(len(res)>=5):
				finalResList = [float(res[i]['cvss']) for i in range(0,5)]
				avgCVSS = sum(finalResList)/len(finalResList)
				return (avgCVSS,res[0]['summary'])
			else:				
				return (res[0]['cvss'],res[0]['summary'])

	except:
		return False

def checkApacheInfo(res):
	numbers = ['0','1','2','3','4','5','6','7','8','9']
	cve =CVESearch()
	try:
		if "Apache" in res['Server']:
			version = res['Server'][7:]
			
			if(len(version)==0):
				version = "2.4.33"
			
			finalVer = ""
			for character in version:
				if((character in numbers) or (character=='.')):
					finalVer = finalVer+character
				else:
					break
			# print(finalVer)
			res = cve.cvefor('cpe:/a:apache:http_server:'+finalVer)
			if(len(res)>=5):
				finalResList = [float(res[i]['cvss']) for i in range(0,5)]
				avgCVSS = sum(finalResList)/len(finalResList)
				return (avgCVSS,res[0]['summary'])
			else:				
				return (res[0]['cvss'],res[0]['summary'])

	except:
		return False

def checkNginxInfo(res):
	numbers = ['0','1','2','3','4','5','6','7','8','9']
	cve =CVESearch()
	try:
		if "nginx" in res['Server']:
			version = res['Server'][4:]
			
			if(len(version)==0):
				version = "1.13.1"

			finalVer = ""
			for character in version:
				if((character in numbers) or (character=='.')):
					finalVer = finalVer+character
				else:
					break
			# print(finalVer)
			res = cve.cvefor('cpe:/a:nginx:nginx'+finalVer)
			if(len(res)>=5):
				finalResList = [float(res[i]['cvss']) for i in range(0,5)]
				avgCVSS = sum(finalResList)/len(finalResList)
				return (avgCVSS,res[0]['summary'])
			else:
				return (res[0]['cvss'],res[0]['summary'])
		else:
			return False
	except:
		return False

if __name__ == '__main__':
	validateServer(sys.argv[1])