import urllib2, urllib
import requests as rq 
# urlList = ["https://expired.badssl.com/","https://wrong.host.badssl.com/","https://self-signed.badssl.com/","https://untrusted-root.badssl.com/","https://sha1-intermediate.badssl.com/","https://rc4.badssl.com/","https://rc4-md5.badssl.com/","https://dh480.badssl.com/","https://dh512.badssl.com/","https://dh1024.badssl.com/","https://superfish.badssl.com/","https://edellroot.badssl.com/","https://dsdtestprovider.badssl.com/","https://preact-cli.badssl.com/","https://webpack-dev-server.badssl.com/","https://null.badssl.com/","https://revoked.badssl.com/","https://pinning-test.badssl.com/","https://invalid-expected-sct.badssl.com/","https://tls-v1-0.badssl.com:1010/","https://tls-v1-1.badssl.com:1011/","https://cbc.badssl.com/","https://3des.badssl.com/","https://sha256.badssl.com/","https://sha384.badssl.com/","https://sha512.badssl.com/","https://rsa2048.badssl.com/","https://ecc256.badssl.com/","https://ecc384.badssl.com/","https://mozilla-modern.badssl.com/","https://1000-sans.badssl.com/","https://10000-sans.badssl.com/","https://rsa8192.badssl.com/","https://no-subject.badssl.com/","https://no-common-name.badssl.com/","https://incomplete-chain.badssl.com/"]

def checkSSL(url):
	# res = urllib.request.urlopen(url).read().decode()
	res = urllib2.urlopen(url).read()
	# print res
	if("bad HPKP pin" in res):
		return (False,u"Invalid SSL certificate, bad HPKP pin.")
	elif("invalid" in res and u"sct" in res):
		return (False,u"Invalid SSL certificate, invalid Signed Certificate Timestamps (SCTs).")
	elif("revoked" in res):
		return (False,u"Invalid SSL certificate, revoked certificate.")
	elif("certificate without a common name." in res):
		return (False,u"This site uses a certificate without a common name.")
	elif("certificate without a subject." in res):
		return (False,u"This site uses a certificate without a subject.")
	elif("uses ephemeral" in res):
		return (False,u"Suggest not using this site, due to shart validity keys.")
	elif("signed using SHA-1" in res):
		return (False,u"SHA-1 used for signing, may not be secure.")
	else:
		try:
			rq.get(url,verify=True)
		except Exception as e:
			# print e
			return (False,e)
		else:
			return (True,u"SSL certificates are Valid.")
