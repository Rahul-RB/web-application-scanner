import urllib.request 

def checkSSL(url):
	# res = urllib.request.urlopen(url).read().decode()
	res = urllib.request.urlopen(url).read()
	print(res)
	if(b"bad HPKP pin" in res):
		print("Invalid SSL certificate, bad HPKP pin.")
		return False
	elif(b"invalid" in res and "sct" in res):
		print("Invalid SSL certificate, invalid Signed Certificate Timestamps (SCTs).")
		return False
	elif(b"revoked" in res):
		print("Invalid SSL certificate, revoked certificate.")
		return False
	elif(b"certificate without a common name." in res):
		print("This site uses a certificate without a common name.")
		return False
	elif(b"certificate without a subject." in res):
		print("This site uses a certificate without a subject.")
		return False
	elif(b"uses ephemeral" in res):
		print("Suggest not using this site, due to shart validity keys.")
		return False
	elif(b"signed using SHA-1" in res):
		print("SHA-1 used for signing, may not be secure.")
		return False
	else:
		print("SSL certificates are Valid.")
		return True
