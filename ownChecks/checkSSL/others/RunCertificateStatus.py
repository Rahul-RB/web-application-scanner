import urllib.request 

# urlList = ["https://expired.badssl.com/","https://wrong.host.badssl.com/","https://self-signed.badssl.com/","https://untrusted-root.badssl.com/","https://sha1-intermediate.badssl.com/","https://rc4.badssl.com/","https://rc4-md5.badssl.com/","https://dh480.badssl.com/","https://dh512.badssl.com/","https://dh1024.badssl.com/","https://superfish.badssl.com/","https://edellroot.badssl.com/","https://dsdtestprovider.badssl.com/","https://preact-cli.badssl.com/","https://webpack-dev-server.badssl.com/","https://null.badssl.com/","https://revoked.badssl.com/","https://pinning-test.badssl.com/","https://invalid-expected-sct.badssl.com/","https://tls-v1-0.badssl.com:1010/","https://tls-v1-1.badssl.com:1011/","https://cbc.badssl.com/","https://3des.badssl.com/","https://sha256.badssl.com/","https://sha384.badssl.com/","https://sha512.badssl.com/","https://rsa2048.badssl.com/","https://ecc256.badssl.com/","https://ecc384.badssl.com/","https://mozilla-modern.badssl.com/","https://1000-sans.badssl.com/","https://10000-sans.badssl.com/","https://rsa8192.badssl.com/","https://no-subject.badssl.com/","https://no-common-name.badssl.com/","https://incomplete-chain.badssl.com/"]

urlList = [
		"https://pinning-test.badssl.com/",
		"https://invalid-expected-sct.badssl.com/",
		"https://revoked.badssl.com/",
		"https://no-common-name.badssl.com/",
		"https://no-subject.badssl.com/",
		"https://dh1024.badssl.com/",
		"https://sha1-intermediate.badssl.com/",
	]
resList = [0]*len(urlList)

def checkSSL(url):
	# res = urllib.request.urlopen(url).read().decode()
	res = urllib.request.urlopen(url).read()
	print("res:",res)
	if(b"bad HPKP pin" in res):
		print("Invalid SSL certificate, bad HPKP pin.")
		return False
	elif(b"invalid" in res and "sct" in res):
		print("Invalid SSL certificate, invalid Signed Certificate Timestamps (SCTs).")
		return False
	elif(b"revoked" in res):
		print("Invalid SSL certificate, revoked certificate.")
		return False
	elif(b"certificate without a common name." in res):
		print("This site uses a certificate without a common name.")
		return False
	elif(b"certificate without a subject." in res):
		print("This site uses a certificate without a subject.")
		return False
	elif(b"uses ephemeral" in res):
		print("Suggest not using this site, due to shart validity keys.")
		return False
	elif(b"signed using SHA-1" in res):
		print("SHA-1 used for signing, may not be secure.")
		return False
	else:
		print("SSL certificates are Valid.")
		return True
	# print("------------------------------------------------------------------------------")

for url in urlList:
	print()
	print()
	print()
	try:
		print(url)
		checkSSL(url)
	except Exception as e:
		# a=str(e)

		print(e)
		print("SSL certificate validation failed.")
	# k=input()