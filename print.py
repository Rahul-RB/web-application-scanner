req = self.Send(url=self.url,method="GET")
s = server(self.kwarg,self.url).run()
if s:plus('Server: %s'%(s))

cms = detectCms(req.headers,req.content)
for c in cms:
	if c != None and c != "":plus('CMS: %s'%(c))

framework = detectFramework(req.headers,req.content)
for f in framework:
	if f != None and f != "":plus('Framework: %s'%(f))

lang = detectLanguage(req.content,self.url)
for l in lang:
	if l != None and l != "":plus('Language: %s'%(l))

os = detectOs(req.headers)
for o in os:
	if o != None and o != "":plus('Operating System: %s'%o)

waf = detectWaf(req.headers,req.content)
for a in waf:
	if a != None and a != "":plus('WAF: %s'%a)

sslRes = checkSSL.checkSSL(url=self.url)
plus("SSL Certificate check result: %s"%sslRes[1])
checkCookieAndHeaders(req.headers,req.content)

	