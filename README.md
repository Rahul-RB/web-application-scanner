# WAScan - Web Application Scanner

## HOW TO RUN:
```python wascan.py --url "127.0.0.1/index1.php?id=1" --scan <GIVE-SCAN-NUMBER-HERE-[0,1,2,3,5]>```

## Features

- __Fingerprint__
	+ Detect Server
	+ Detect Web Frameworks (22)
	+ Check Cookie Security  
	+ Check SSL Certificate validity  
	+ Check Headers Security 
	+ Detect Language (9)
	+ Detect Operating System (OS - 8)
	+ Detect Content Management System (CMS - 6)
	+ Detect Web Application Firewall (WAF - 54) 

- __Attacks__
	+ Bash Command Injection (ShellShock)
	+ Blind SQL Injection
	+ SQL Injection via Cookie,Referer and User-Agent Header Value
	+ Cross-Site Scripting (XSS) via Cookie,Referer and User-Agent Header Value
	+ Buffer Overflow
	+ HTML Code Injection 
	+ PHP Code Injection
	+ LDAP Injection 
	+ Local File Inclusion (lfi)
	+ OS Commanding
	+ SQL Injection 
	+ XPath Injection 
	+ Cross Site Scripting (XSS)

- __Audit__
	+ Apache Status
	+ WebDav 
	+ PHPInfo
	+ Robots Paths
	+ Cross-Site Tracing (XST)

- __Bruteforce__
	+ Admin Panel 
	+ Backdoor (shell) 
	+ Backup Dirs
	+ Backup Files 
	+ Common Dirs
	+ Common Files

- __Disclosure__
	+ Credit Cards 
	+ Emails
	+ Private IP 
	+ SSN
	+ Detect Warnings,Fatal Error,...

