from re import search,I

def codeigniter(headers,content):
	_ = False
	for header in headers.items():
		_ |= search("ci_session=",header[1]) is not None
		if _ : break
	if _ : return "CodeIgniter - PHP Framework"