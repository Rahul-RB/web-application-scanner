from re import search,I

def dancer(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r"Dancer|dancer.session=",header[1]) is not None
		if _ : break
	if _ : return "Dancer - Perl Framework"