from re import search,I

def airlock(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'\AAL[_-]?(SESS|LB)=',header[1],I) is not None
		if _ : break 
	if _:
		return "Airlock (Phion/Ergon)" 