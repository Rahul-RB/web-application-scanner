from re import search,I

def comodo(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'protected by comodo waf',header[1],I) is not None
		if _: break
	if _ : 
		return "Comodo Web Application Firewall (Comodo)"