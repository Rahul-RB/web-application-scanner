from re import search,I

def ciscoacexml(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'ace xml gateway',header[1],I) is not None
		if _: break
	if _ : 
		return "Cisco ACE XML Gateway (Cisco Systems)"