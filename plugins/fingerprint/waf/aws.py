from re import search,I

def aws(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'\bAWS',header[1],I) is not None
	if _: 
		return "Amazon Web Services Web Application Firewall (Amazon)" 