

from re import search,I 

def fortiweb(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'fortiwafsid=',header[1],I) is not None
		if _: break
	if _ : 
		return "FortiWeb Web Application Firewall (Fortinet)"