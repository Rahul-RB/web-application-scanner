from re import search,I

def dotdefender(headers,content):
	_ = False
	for header in headers.items():
		_ |= header[0] == "x-dotdefender-denied"
		if _: break
	_ |= search(r"dotDefender Blocked Your Request",content) is not None
	if _ : 
		return "dotDefender (Applicure Technologies)"