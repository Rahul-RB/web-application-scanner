from re import search,I

def barracuda(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'\Abarra_counter_session=|(\A|\b)barracuda_',header[1],I) is not None
		if _ : break
	if _:
		return "Barracuda Web Application Firewall (Barracuda Networks)"