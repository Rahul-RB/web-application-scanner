

from re import search,I 

def hyperguard(headers,content):
	_ = False
	for header in headers.items():
		_ |= search(r'odsession=',header[1],I) is not None
		if _: break
	if _ :
		return "Hyperguard Web Application Firewall (art of defence)"