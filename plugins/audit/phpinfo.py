

from os import path
from re import search
from lib.utils.check import *
from lib.utils.printer import *
from lib.utils.readfile import *
from lib.request.request import *

class phpinfo(Request):
	""" phpinfo  """
	get = "GET"
	def __init__(self,kwargs,url,data):
		Request.__init__(self,kwargs)
		self.url = url 
		self.data = data

	def search(self):
		""" Search Path """
		_ = None
		realpath = path.join(path.realpath(__file__).split('plugins')[0],'lib/db/')
		return (realpath+"phpinfo.wascan")

	def run(self):
		""" Run """
		for path in readfile(self.search()):
			# check url path
			url = CPath(self.url,path)
			# send request 
			# print "url:",url
			req = self.Send(url=url,method=self.get,)
			# print "req.code:",req.code
			# print "-------------------------------------"
			# print "req.content:",req.content
			# if status code == 200
			if req.code == 200:
				# and search in req.content
				if search(r'\<title\>phpinfo()\<\/title\>|\<h1 class\=\"p\"\>PHP Version',req.content):
					plus('Found phpinfo page at: %s'%(req.url))
					break
		# plus('No phpinfo() vulnerability')