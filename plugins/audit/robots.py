

from re import findall,I
from lib.utils.check import *
from lib.utils.printer import *
from lib.utils.readfile import *
from lib.request.request import *

class robots(Request):
	""" check robots path  """
	get = "GET"
	def __init__(self,kwargs,url,data):
		Request.__init__(self,kwargs)
		self.url = url 
		self.data = data

	def run(self):
		""" Run """
		plus('Checking robots paths...')
		paths = []
		# check url path
		url = CPath(self.url,'robots.txt')
		# send request
		req = self.Send(url=url,method=self.get)
		# if status code == 200 and req.url == url
		if req.code == 200 and CEndUrl(req.url) == url:
			# and req.content != ""
			if req.content != "":
				# findall all robots (allow:.../disallow:...) paths
				paths += findall(r'[disallow]\: (\S*)',req.content)
				print "Paths found: Check for any vulnerable info in the files:"
				print paths
		if paths != None and paths != "":
			for path in paths:
				# check url path
				url = CPath(self.url,path)
				# send request 
				req = self.Send(url=url,method=self.get)
				# print code and url
				more('[%s] %s'%(req.code,req.url))