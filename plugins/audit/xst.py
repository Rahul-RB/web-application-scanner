

from re import search,I
from lib.utils.printer import *
from lib.request.request import *
import subprocess

class xst(Request):
	""" dav """
	# method
	trace = "TRACE"
	def __init__(self,kwargs,url,data):
		Request.__init__(self,kwargs)
		self.url = url
		self.data = data

	def run(self):
		"""Run"""
		# headers 
		# headers = {
		# 			'menace':'Hello_Word'
		# 			}
		# # send request 
		# req = self.Send(url=self.url,method=self.trace,headers=headers)
		# # 
		# regexp = r"*?hello_word$"
		# if 'menace' in req.headers or 'menace' in req.headers:
		# 	if search(regexp,req.headers['menace'],I):
		# 		plus('This site is vulnerabile to Cross Site Tracing (XST) at: %s'%(req.url))
		cmd = """curl -X TRACE"""+self.url
		args = cmd.split()
		process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = process.communicate()

		if "ACCEPT" in stdout:
			plus('This site is vulnerabile to Cross Site Tracing (XST)')
		else:
			plus('Not vulnerable to XST')